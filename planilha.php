<?php

// Incluimos a classe PHPExcel
include  'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';

// Instanciamos a classe
$objPHPExcel = new PHPExcel();

// Definimos o estilo da fonte
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

// Criamos as colunas
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'codestacao' )
            ->setCellValue('B1', "latitude" )
            ->setCellValue("C1", "longitude" )
            ->setCellValue("D1", "cidade" )
            ->setCellValue("E1", "nome" )
            ->setCellValue("F1", "tipo" )
            ->setCellValue("G1", "uf" )
            ->setCellValue("H1", "chuva" )
            ->setCellValue("I1", "dataHora" );

$linha = 2;
$coluna = 0;
$result = file_get_contents('http://150.163.255.240/CEMADEN/resources/parceiros/GO/1');
$json = json_decode($result); 
foreach ($json as $key => $value) {
	foreach ($value as $k => $v) {
		foreach ($v as $j => $y) {
			if($coluna >= 9)
				$coluna = 0;

			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($coluna, $linha, $y);
			$coluna++;
		}
		$linha++;
	}
}


// Podemos renomear o nome das planilha atual, lembrando que um único arquivo pode ter várias planilhas
$objPHPExcel->getActiveSheet()->setTitle('Tabela arcgis');

// Cabeçalho do arquivo para ele baixar
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename=testefinal.csv');
header('Cache-Control: max-age=0');
// Se for o IE9, isso talvez seja necessário
header('Cache-Control: max-age=1');

// Acessamos o 'Writer' para poder salvar o arquivo
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
// $objWriter->setEnclosure( '"' );
// Salva diretamente no output, poderíamos mudar arqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
$objWriter->save('php://output'); 

exit;

?>